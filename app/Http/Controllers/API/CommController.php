<?php
/**
 * Created by PhpStorm.
 * User: msp
 * Date: 27/08/18
 * Time: 6:52 PM
 */

namespace App\Http\Controllers\API;


use App\Model\Comm;
use App\Model\FComm;
use App\Model\FUser;
use App\Model\FUserDetail;
use App\Model\MComm;
use App\Model\MUser;
use App\Model\MUserDetail;
use App\Model\User;
use App\Notifications\ContactRequestRespond;
use App\Notifications\ContactRequestSent;
use App\Notifications\InterestMessageRespond;
use App\Notifications\InterestMessageSent;
use App\Notifications\PhotoRequestRespond;
use App\Notifications\PhotoRequestSent;
use App\Notifications\ProfileVisits;
use App\Notifications\Shortlisted;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommController extends Controller
{

    public function commDetails(Request $request, $type) {
        $subtype = 0;

        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();

        $pageSize = config('constants.page_size');

        $lockedPhotoUrl = config('constants.locked_photo_url');
        $photoUrl =  url('storage/photos')."/";

        User::$commClass = $user->commAltClass();
        User::$commAltClass = $user->commClass();

        $altUserDetailTable = $user->detailAltTable();
        $commTable = $user->commTable();
        $altCommTable = $user->commAltTable();

        $commColumn = 'profileVisits';
        $commColumnDate = 'profileVisitsDate';
        $message = "";

        switch($type) {
            case Comm::TYPE_PV_I_VISITED:
                $subtype = 1;
                $commColumn = 'profileVisits';
                $commColumnDate = 'profileVisitsDate';
                $message = "You have visited profile of the following users";
                break;
            case Comm::TYPE_PV_VISITED_ME:
                $subtype = 0;
                $commColumn = 'profileVisits';
                $commColumnDate = 'profileVisitsDate';
                $message = "The following users have visited your profile";
                break;

            case Comm::TYPE_IM_SENT:
                $subtype = 1;
                $commColumn = 'interestMessage';
                $commColumnDate = 'interestMessageDate';
                $message = "You have send interest messages to the following users";
                break;
            case Comm::TYPE_IM_RECEIVED:
                $subtype = 0;
                $commColumn = 'interestMessage';
                $commColumnDate = 'interestMessageDate';
                $message = "The following users have send you an interest message";
                break;

            case Comm::TYPE_PR_SENT:
                $subtype = 1;
                $commColumn = 'photoRequest';
                $commColumnDate = 'photoRequestDate';
                $message = "You have requested photos of the following users";
                break;
            case Comm::TYPE_PR_RECEIVED:
                $subtype = 0;
                $commColumn = 'photoRequest';
                $commColumnDate = 'photoRequestDate';
                $message = "The following users have requested your photo";
                break;

            case Comm::TYPE_CR_SENT:
                $subtype = 1;
                $commColumn = 'contactRequest';
                $commColumnDate = 'contactRequestDate';
                $message = "You have requested contacts of the following users";
                break;
            case Comm::TYPE_CR_RECEIVED:
                $subtype = 0;
                $commColumn = 'contactRequest';
                $commColumnDate = 'contactRequestDate';
                $message = "The following users have requested your contact";
                break;
        }

        if($subtype == 0) {
            //visitedMeUsers
            $users = User::select(
                'users.*',
//                DB::raw("(CASE WHEN users.photoLock = 1 AND ($altCommTable.prStatus != '".Comm::STATUS_APPROVED."' OR $commTable.prStatus != '".Comm::STATUS_APPROVED."') THEN '$lockedPhotoUrl' ELSE users.mainPhotoLink END) AS mainPhotoLink"),
                DB::raw("(CASE WHEN users.photoLock = 0 OR $altCommTable.prStatus = '".Comm::STATUS_APPROVED."' OR $commTable.prStatus = '".Comm::STATUS_APPROVED."' THEN CONCAT('$photoUrl', users.mainPhotoLink) ELSE '$lockedPhotoUrl' END) AS mainPhotoLink"),
                DB::raw("(CASE WHEN users.photoLock = 0 OR $altCommTable.prStatus = '".Comm::STATUS_APPROVED."' OR $commTable.prStatus = '".Comm::STATUS_APPROVED."' THEN 1 ELSE 0 END) AS photoLock"),
                $altUserDetailTable . '.birthDate',
                $altUserDetailTable . '.height',
                $altUserDetailTable . '.educationLevel',
                $altUserDetailTable . '.graduationSubject',
                $altUserDetailTable . '.masterSubject',
                $altUserDetailTable . '.job',
				$altUserDetailTable . '.caste',
				$altUserDetailTable . '.subCaste',
				$altUserDetailTable . '.financialStatus',
                $altUserDetailTable . '.currentCity',
				$altUserDetailTable . '.currentTaluk',
				$altUserDetailTable . '.currentDistrict',
                $altUserDetailTable . '.currentState',
                $altUserDetailTable . '.currentCountry',
				$altUserDetailTable . '.familyCity',
				$altUserDetailTable . '.familyTaluk',
				$altUserDetailTable . '.familyDistrict',
                $altUserDetailTable . '.familyState',
                $altUserDetailTable . '.familyCountry',
                $altUserDetailTable . '.accountStatus')
                ->with(['comm', 'commAlt'])
                ->join($altCommTable, $altCommTable . '.userId1', 'users.id')
                ->join($altUserDetailTable, $altUserDetailTable . '.userId', 'users.id')
                ->leftJoin($commTable, function($join) use($commTable, $id) {
                    $join->on($commTable . '.userId2', 'users.id');
                    $join->where($commTable . '.userId1', $id);
                })
                ->where($altCommTable . '.userId2', $id)
//                ->where($altCommTable . '.'.$commColumn, '!=', 0)
                ->where(function($query) use ($altCommTable, $commColumn) {
                    $query->where($altCommTable . '.'.$commColumn, 1);
                    $query->orWhere($altCommTable . '.'.$commColumn, 2);
                })
                ->where($altCommTable . '.contact_status', '!=', Comm::CONTACT_STATUS_IGNORED)
                ->where(function($query) use ($commTable) {
                    $query->whereNull($commTable . '.contact_status');
                    $query->orWhere($commTable . '.contact_status', "!=", Comm::CONTACT_STATUS_IGNORED);
                })
                ->orderBy($altCommTable . '.'.$commColumnDate, 'desc')
                ->simplePaginate($pageSize);
        } else {
            //iVisitedUsers
            $users = User::select(
                'users.*',
//                DB::raw("(CASE WHEN users.photoLock = 1 AND ($altCommTable.prStatus != '".Comm::STATUS_APPROVED."' OR $commTable.prStatus != '".Comm::STATUS_APPROVED."') THEN '$lockedPhotoUrl' ELSE users.mainPhotoLink END) AS mainPhotoLink"),
                DB::raw("(CASE WHEN users.photoLock = 0 OR $altCommTable.prStatus = '".Comm::STATUS_APPROVED."' OR $commTable.prStatus = '".Comm::STATUS_APPROVED."' THEN CONCAT('$photoUrl', users.mainPhotoLink) ELSE '$lockedPhotoUrl' END) AS mainPhotoLink"),
                DB::raw("(CASE WHEN users.photoLock = 0 OR $altCommTable.prStatus = '".Comm::STATUS_APPROVED."' OR $commTable.prStatus = '".Comm::STATUS_APPROVED."' THEN 1 ELSE 0 END) AS photoLock"),
                $altUserDetailTable . '.birthDate',
                $altUserDetailTable . '.height',
                $altUserDetailTable . '.educationLevel',
                $altUserDetailTable . '.graduationSubject',
                $altUserDetailTable . '.masterSubject',
                $altUserDetailTable . '.job',
				$altUserDetailTable . '.caste',
				$altUserDetailTable . '.subCaste',
				$altUserDetailTable . '.financialStatus',
                $altUserDetailTable . '.currentCity',
				$altUserDetailTable . '.currentTaluk',
				$altUserDetailTable . '.currentDistrict',
                $altUserDetailTable . '.currentState',
                $altUserDetailTable . '.currentCountry',
				$altUserDetailTable . '.familyCity',
				$altUserDetailTable . '.familyTaluk',
				$altUserDetailTable . '.familyDistrict',
                $altUserDetailTable . '.familyState',
                $altUserDetailTable . '.familyCountry',
                $altUserDetailTable . '.accountStatus')
                ->with(['comm', 'commAlt'])
                ->join($commTable, $commTable . '.userId2', 'users.id')
                ->join($altUserDetailTable, $altUserDetailTable . '.userId', 'users.id')
                ->leftJoin($altCommTable, function($join) use($altCommTable, $id) {
                    $join->where($altCommTable . '.userId1', 'users.id');
                    $join->where($altCommTable . '.userId2', $id);
                })
                ->where($commTable . '.userId1', $id)
//                ->where($commTable . '.'.$commColumn, '!=', 0)
                ->where(function($query) use ($commTable, $commColumn) {
                    $query->where($commTable . '.'.$commColumn, 1);
                    $query->orWhere($commTable . '.'.$commColumn, 2);
                })
                ->where($commTable . '.contact_status', "!=", Comm::CONTACT_STATUS_IGNORED)
                ->where(function($query) use ($altCommTable) {
                    $query->whereNull($altCommTable . '.contact_status');
                    $query->orWhere($altCommTable . '.contact_status', "!=", Comm::CONTACT_STATUS_IGNORED);
                })
                ->orderBy($commTable . '.'.$commColumnDate, 'desc')
                ->simplePaginate($pageSize);
        }

        return $this->success($message, $users);
    }

    public function profileVisits(Request $request, $type) {
        return $this->commDetails($request, $type == 'visited-me' ? Comm::TYPE_PV_VISITED_ME : Comm::TYPE_PV_I_VISITED);
    }

    public function interestMessages(Request $request, $type) {
        return $this->commDetails($request, $type == 'received' ? Comm::TYPE_IM_RECEIVED : Comm::TYPE_IM_SENT);
    }

    public function photoRequest(Request $request, $type) {
        return $this->commDetails($request, $type == 'received' ? Comm::TYPE_PR_RECEIVED : Comm::TYPE_PR_SENT);
    }

    public function contactRequest(Request $request, $type) {
        return $this->commDetails($request, $type == 'received' ? Comm::TYPE_CR_RECEIVED : Comm::TYPE_CR_SENT);
    }

    public function shortlistedUsers() {
        /** @var User $user */
        $user = Auth::user();
        $commTable = $user->commTable();
        $altCommTable = $user->commAltTable();
        $altUserDetailTable = $user->detailAltTable();
        $pageSize = config('constants.page_size');
        $lockedPhotoUrl = config('constants.locked_photo_url');
        $photoUrl =  url('storage/photos')."/";

        User::$commClass = $user->commAltClass();
        User::$commAltClass = $user->commClass();

        $shortlisted = User::select(
            'users.*',
//            DB::raw("(CASE WHEN users.photoLock = 1 AND ($altCommTable.prStatus != '".Comm::STATUS_APPROVED."' OR $commTable.prStatus != '".Comm::STATUS_APPROVED."') THEN '$lockedPhotoUrl' ELSE users.mainPhotoLink END) AS mainPhotoLink"),
            DB::raw("(CASE WHEN users.photoLock = 0 OR $altCommTable.prStatus = '".Comm::STATUS_APPROVED."' OR $commTable.prStatus = '".Comm::STATUS_APPROVED."' THEN CONCAT('$photoUrl', users.mainPhotoLink) ELSE '$lockedPhotoUrl' END) AS mainPhotoLink"),
            DB::raw("(CASE WHEN users.photoLock = 0 OR $altCommTable.prStatus = '".Comm::STATUS_APPROVED."' OR $commTable.prStatus = '".Comm::STATUS_APPROVED."' THEN 1 ELSE 0 END) AS photoLock"),
            $altUserDetailTable . '.birthDate',
            $altUserDetailTable . '.height',
            $altUserDetailTable . '.educationLevel',
            $altUserDetailTable . '.graduationSubject',
            $altUserDetailTable . '.masterSubject',
            $altUserDetailTable . '.job',
			$altUserDetailTable . '.caste',
			$altUserDetailTable . '.subCaste',
			$altUserDetailTable . '.financialStatus',
			$altUserDetailTable . '.currentCity',
			$altUserDetailTable . '.currentTaluk',
			$altUserDetailTable . '.currentDistrict',
			$altUserDetailTable . '.currentState',
			$altUserDetailTable . '.currentCountry',
			$altUserDetailTable . '.familyCity',
			$altUserDetailTable . '.familyTaluk',
			$altUserDetailTable . '.familyDistrict',
			$altUserDetailTable . '.familyState',
			$altUserDetailTable . '.familyCountry',
            $altUserDetailTable . '.accountStatus')
            ->with(['comm', 'commAlt'])
            ->join($commTable, $commTable . '.userId2', 'users.id')
            ->join($altUserDetailTable, $altUserDetailTable . '.userId', 'users.id')
            ->leftJoin($altCommTable, function($join) use($altCommTable, $user) {
                $join->where($altCommTable . '.userId1', 'users.id');
                $join->where($altCommTable . '.userId2', $user->id);
            })
            ->where($commTable . '.userId1', $user->id)
            ->where($commTable . '.contact_status', Comm::CONTACT_STATUS_SHORTLISTED)
            ->orderBy($altUserDetailTable . '.accountStatus', 'desc')
            ->simplePaginate($pageSize);

        return $this->success('Fetched shortlisted users', $shortlisted);
    }

    public function shortlist($userId) {
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();
        $isMale = $user->isMale();

        $commClass = $isMale ? MComm::class : FComm::class;
        $comm = $commClass::where('userId1', $id)->where('userId2', $userId)->first();

        if(is_null($comm)) {
            $comm = new $commClass();
            $comm->userId1 = $id;
            $comm->userId2 = $userId;
        }

        $comm->contact_status = Comm::CONTACT_STATUS_SHORTLISTED;
        $comm->save();

        $commAlt = $commClass::where('userId1', $userId)->where('userId2', $id)->first();
        if(!is_null($commAlt) && $commAlt->contact_status == Comm::CONTACT_STATUS_SHORTLISTED) {
            /** @var User $partner */
            $partner = User::find($userId);
            if(!is_null($partner)) {
                $partner->notify(new Shortlisted($user));
            }
        }

        return $this->success("Your have successfully shortlisted", $comm);
    }

    public function ignore(Request $request, $userId) {
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();
        $isMale = $user->isMale();

        $commClass = $isMale ? MComm::class : FComm::class;
        $comm = $commClass::where('userId1', $id)->where('userId2', $userId)->first();

        if(is_null($comm)) {
            $comm = new $commClass();
            $comm->userId1 = $id;
            $comm->userId2 = $userId;
        }

        $comm->contact_status = $request->isMethod('POST') ? Comm::CONTACT_STATUS_IGNORED : Comm::CONTACT_STATUS_NONE;
        $comm->save();
        return $this->success("Your have successfully contact_status", $comm);
    }

    public function storePV($userId) {
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();
        $isMale = $user->isMale();

        $commClass = $isMale ? MComm::class : FComm::class;
        /** @var MComm|FComm $comm */
        $comm = $commClass::where('userId1', $id)->where('userId2', $userId)->first();

        if(!is_null($comm) && $comm->profileVisits != 0) {
            return $this->error("Profile visits already marked", Response::HTTP_NOT_MODIFIED);
        }

        if(is_null($comm)) {
            $comm = new $commClass;
            $comm->userId1 = $id;
            $comm->userId2 = $userId;
        }

        $comm->profileVisits = 1;
        $comm->profileVisitsDate = Carbon::now()->toDateTimeString();
        $comm->save();

        /** @var User $partner */
        $partner = User::find($userId);
        if(!is_null($partner)) {
            $partner->notify(new ProfileVisits($user));
        }

        return $this->success("Your have successfully marked profile visits", $comm);
    }

    public function markAsReadPV($userId) {
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();
        $isMale = $user->isMale();

        $commClass = !$isMale ? MComm::class : FComm::class;
        $comm = $commClass::where('userId1', $userId)->where('userId2', $id)->where('profileVisits', '!=', 0)->first();

        if(is_null($comm)) {
            return $this->error("Profile visits not found", 404);
        }

        $comm->profileVisits = 2;
        $comm->save();
        return $this->success("Your have successfully marked as read profile visits", $comm);
    }

    public function sendIM($userId) {
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();
        $isMale = $user->isMale();

        $commAltClass = !$isMale ? MComm::class : FComm::class;
        /** @var MComm|FComm $commAlt */
        $commAlt = $commAltClass::where('userId1', $userId)->where('userId2', $id)->where('interestMessage', true)->first();

        if(!is_null($commAlt)) {
            return $this->success("Interest message received", $commAlt, Response::HTTP_ALREADY_REPORTED);
        }

        $commClass = $isMale ? MComm::class : FComm::class;
        /** @var MComm|FComm $comm */
        $comm = $commClass::where('userId1', $id)->where('userId2', $userId)->first();

        if(!is_null($comm) && $comm->interestMessage == 1) {
            return $this->success("Interest message sent", $comm);
        }

        if(is_null($comm)) {
            $comm = new $commClass;
            $comm->userId1 = $id;
            $comm->userId2 = $userId;
        }

        $comm->interestMessage = 1;
        $comm->interestMessageDate = Carbon::now()->toDateTimeString();
        $comm->imStatus = Comm::STATUS_UNREAD;
        $comm->imStatusDate = Carbon::now()->toDateTimeString();
        $comm->save();

        /** @var User $partner */
        $partner = User::find($userId);
        if(!is_null($partner)) {
            $partner->notify(new InterestMessageSent($user));
        }

        return $this->success("Your have successfully sent interest message", $comm);
    }

    public function cancelIM($userId) {
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();
        $isMale = $user->isMale();

        $commClass = $isMale ? MComm::class : FComm::class;
        /** @var MComm|FComm $comm */
        $comm = $commClass::where('userId1', $id)->where('userId2', $userId)->where('interestMessage', true)->first();

        if(is_null($comm)) {
            return $this->error("Interest message not found", Response::HTTP_NOT_FOUND);
        }

        $comm->interestMessage = 0;
        $comm->interestMessageDate = null;
        $comm->imStatus = Comm::STATUS_UNREAD;
        $comm->imStatusDate = null;
        $comm->save();

        return $this->success("Your have successfully canceled interest message you sent", $comm);
    }

    public function updateIMStatus($userId, $status) {
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();
        $isMale = $user->isMale();

        $commClass = !$isMale ? MComm::class : FComm::class;
        /** @var MComm|FComm $comm */
        $comm = $commClass::where('userId1', $userId)->where('userId2', $id)->where('interestMessage', true)->first();

        if(is_null($comm)) {
            return $this->error("Interest message not found", Response::HTTP_NOT_FOUND);
        }

        if($status == 'pending' && $comm->imStatus != Comm::STATUS_UNREAD) {
            return $this->success("Your have already read interest message", $comm);
        } else if($comm->imStatus == $status) {
            return $this->success("Your have already $status interest message", $comm);
        }

        $comm->imStatus = ucfirst($status);
        $comm->imStatusDate = Carbon::now()->toDateTimeString();
        $comm->save();

        if($status != 'pending') {
            /** @var User $partner */
            $partner = User::find($userId);
            if(!is_null($partner)) {
                $partner->notify(new InterestMessageRespond($user, $comm));
            }
        }

        return $this->success("Your have successfully $status interest message", $comm);
    }

    public function sendPR($userId) {
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();
        $isMale = $user->isMale();

        $commAltClass = !$isMale ? MComm::class : FComm::class;
        /** @var MComm|FComm $commAlt */
        $commAlt = $commAltClass::where('userId1', $userId)->where('userId2', $id)->where('photoRequest', true)->first();

        if(!is_null($commAlt)) {
            return $this->success("Photo request received", $commAlt, Response::HTTP_ALREADY_REPORTED);
        }

        $commClass = $isMale ? MComm::class : FComm::class;
        /** @var MComm|FComm $comm */
        $comm = $commClass::where('userId1', $id)->where('userId2', $userId)->first();

        if(!is_null($comm) && $comm->photoRequest == 1) {
            return $this->success("Photo request sent", $comm);
        }

        if(is_null($comm)) {
            $comm = new $commClass;
            $comm->userId1 = $id;
            $comm->userId2 = $userId;
        }

        $comm->photoRequest = 1;
        $comm->photoRequestDate = Carbon::now()->toDateTimeString();
        $comm->prStatus = Comm::STATUS_UNREAD;
        $comm->prStatusDate = Carbon::now()->toDateTimeString();
        $comm->save();

        /** @var User $partner */
        $partner = User::find($userId);
        if(!is_null($partner)) {
            $partner->notify(new PhotoRequestSent($user));
        }

        return $this->success("Your have successfully sent photo request", $comm);
    }

    public function cancelPR($userId) {
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();
        $isMale = $user->isMale();

        $commClass = $isMale ? MComm::class : FComm::class;
        /** @var MComm|FComm $comm */
        $comm = $commClass::where('userId1', $id)->where('userId2', $userId)->where('photoRequest', true)->first();

        if(is_null($comm)) {
            return $this->error("Photo request not found", Response::HTTP_NOT_FOUND);
        }

        $comm->photoRequest = 0;
        $comm->photoRequestDate = null;
        $comm->prStatus = Comm::STATUS_UNREAD;
        $comm->prStatusDate = null;
        $comm->save();

        return $this->success("Your have successfully canceled photo request you sent", $comm);
    }

    public function updatePRStatus($userId, $status) {
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();
        $isMale = $user->isMale();

        $commClass = !$isMale ? MComm::class : FComm::class;
        $comm = $commClass::where('userId1', $userId)->where('userId2', $id)->where('photoRequest', true)->first();

        if(is_null($comm)) {
            return $this->error("Photo request not found", 404);
        }

        if($status == 'pending' && $comm->prStatus != Comm::STATUS_UNREAD) {
            return $this->success("Your have already read photo request", $comm);
        }

        $comm->prStatus = ucfirst($status);
        $comm->prStatusDate = Carbon::now()->toDateTimeString();
        $comm->save();

        if($status != 'pending') {
            /** @var User $partner */
            $partner = User::find($userId);
            if(!is_null($partner)) {
                $partner->notify(new PhotoRequestRespond($user, $comm));
            }
        }

        return $this->success("Your have successfully $status photo request", $comm);
    }

    public function sendCR($userId) {
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();
        $isMale = $user->isMale();

        $commAltClass = !$isMale ? MComm::class : FComm::class;
        /** @var MComm|FComm $commAlt */
        $commAlt = $commAltClass::where('userId1', $userId)->where('userId2', $id)->where('contactRequest', true)->first();

        if(!is_null($commAlt)) {
            return $this->success("Contact request received", $commAlt, Response::HTTP_ALREADY_REPORTED);
        }

        $commClass = $isMale ? MComm::class : FComm::class;
        /** @var MComm|FComm $comm */
        $comm = $commClass::where('userId1', $id)->where('userId2', $userId)->first();

        if(!is_null($comm) && $comm->contactRequest == 1) {
            return $this->success("Contact request sent", $comm);
        }

        if(is_null($comm)) {
            $comm = new $commClass;
            $comm->userId1 = $id;
            $comm->userId2 = $userId;
        }

        $comm->contactRequest = 1;
        $comm->contactRequestDate = Carbon::now()->toDateTimeString();
        $comm->crStatus = Comm::STATUS_UNREAD;
        $comm->crStatusDate = Carbon::now()->toDateTimeString();
        $comm->save();

        /** @var User $partner */
        $partner = User::find($userId);
        if(!is_null($partner)) {
            $partner->notify(new ContactRequestSent($user));
        }

        return $this->success("Your have successfully sent photo request", $comm);
    }

    public function cancelCR($userId) {
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();
        $isMale = $user->isMale();

        $commClass = $isMale ? MComm::class : FComm::class;
        /** @var MComm|FComm $comm */
        $comm = $commClass::where('userId1', $id)->where('userId2', $userId)->where('contactRequest', true)->first();

        if(is_null($comm)) {
            return $this->error("Contact request not found", Response::HTTP_NOT_FOUND);
        }

        $comm->contactRequest = 0;
        $comm->contactRequestDate = null;
        $comm->crStatus = Comm::STATUS_UNREAD;
        $comm->crStatusDate = null;
        $comm->save();

        return $this->success("Your have successfully canceled Contact request you sent", $comm);
    }

    public function updateCRStatus($userId, $status) {
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();
        $isMale = $user->isMale();

        $commClass = !$isMale ? MComm::class : FComm::class;
        $comm = $commClass::where('userId1', $userId)->where('userId2', $id)->where('contactRequest', true)->first();

        if(is_null($comm)) {
            return $this->error("Contact request not found", 404);
        }

        if($status == 'pending' && $comm->crStatus != Comm::STATUS_UNREAD) {
            return $this->success("Your have already read contact request", $comm);
        }

        $comm->crStatus = ucfirst($status);
        $comm->crStatusDate = Carbon::now()->toDateTimeString();
        $comm->save();

        if($status != 'pending') {
            /** @var User $partner */
            $partner = User::find($userId);
            if(!is_null($partner)) {
                $partner->notify(new ContactRequestRespond($user, $comm));
            }
        }

        return $this->success("Your have successfully $status contact request", $comm);
    }
}
