<?php

namespace App\Http\Controllers\API;

use App\Model\Comm;
use App\Model\FUserDetail;
use App\Model\MUserDetail;
use App\Model\User;
use App\Model\UserPartnerPref;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MyMatchesController extends Controller
{
    public function index() {
        $id = Auth::id();

        /** @var User $user */
        $user = Auth::user();

        /** @var MUserDetail|FUserDetail $detail */
        $detail = $user->detail;

        /** @var UserPartnerPref $partnerPref */
        $partnerPref = $user->partnerPref;

        $pageSize = config('constants.page_size');
        $lockedPhotoUrl = config('constants.locked_photo_url');
        $photoUrl =  url('storage/photos')."/";

        $altDetailTable = $user->detailAltTable();

        $commTable = $user->commTable();
        $altCommTable = $user->commAltTable();

        User::$commClass = $user->commAltClass();
        User::$commAltClass = $user->commClass();

        /** @var Builder $users */
        $users = User::select(
            'users.*',
//            DB::raw("(CASE WHEN users.photoLock = 1 AND ($altCommTable.prStatus != '".Comm::STATUS_APPROVED."' OR $commTable.prStatus != '".Comm::STATUS_APPROVED."') THEN '$lockedPhotoUrl' ELSE users.mainPhotoLink END) AS mainPhotoLink"),
            DB::raw("(CASE WHEN users.photoLock = 0 OR $altCommTable.prStatus = '".Comm::STATUS_APPROVED."' OR $commTable.prStatus = '".Comm::STATUS_APPROVED."' THEN CONCAT('$photoUrl', users.mainPhotoLink) ELSE '$lockedPhotoUrl' END) AS mainPhotoLink"),
            DB::raw("(CASE WHEN users.photoLock = 0 OR $altCommTable.prStatus = '".Comm::STATUS_APPROVED."' OR $commTable.prStatus = '".Comm::STATUS_APPROVED."' THEN 1 ELSE 0 END) AS photoLock"),
            'user_details.birthDate',
            'user_details.height',
            'user_details.educationLevel',
            'user_details.graduationSubject',
            'user_details.masterSubject',
            'user_details.job',
			'user_details.caste',
			'user_details.subCaste',
            'user_details.financialStatus',
            'user_details.currentCity',
			'user_details.currentTaluk',
			'user_details.currentDistrict',
            'user_details.currentState',
            'user_details.currentCountry',
			'user_details.familyCity',
			'user_details.familyTaluk',
			'user_details.familyDistrict',
			'user_details.familyState',
			'user_details.familyCountry',
            'user_details.accountStatus')
            ->with(['comm', 'commAlt'])
            ->join($altDetailTable . ' as user_details', 'user_details.userId', 'users.id')
            ->join('user_partner_prefs', 'user_partner_prefs.userId', 'users.id')
            ->leftJoin($commTable, function(JoinClause $join) use($commTable, $user) {
                $join->on($commTable . '.userId2', 'users.id');
                $join->where($commTable . '.userId1', $user->id);
            })
            ->leftJoin($altCommTable, function(JoinClause $join) use($altCommTable, $user) {
                $join->on($altCommTable . '.userId1', 'users.id');
                $join->where($altCommTable . '.userId2', $user->id);
            })
            ->where(function(Builder $query) use ($commTable) {
                $query->whereNull($commTable . '.contact_status');
                $query->orWhere($commTable . '.contact_status', "!=", Comm::CONTACT_STATUS_IGNORED);
            })
            ->where(function(Builder $query) use ($altCommTable) {
                $query->whereNull($altCommTable . '.contact_status');
                $query->orWhere($altCommTable . '.contact_status', "!=", Comm::CONTACT_STATUS_IGNORED);
            });

        // age
        $age = date_diff(date_create($detail->birthDate), date_create('today'))->y;
        $users->whereBetween(DB::raw($age), [DB::raw('user_partner_prefs.partnerMinAge'), DB::raw('user_partner_prefs.partnerMaxAge')]);
        $users->whereBetween(DB::raw('TIMESTAMPDIFF(YEAR, user_details.birthDate, CURDATE())'), [$partnerPref->partnerMinAge, $partnerPref->partnerMaxAge]);

        // height
        $height = substr($detail->height, strpos($detail->height, ' - ') + 3, 3);
        $partnerMinHeight = substr($partnerPref->partnerMinHeight, strpos($partnerPref->partnerMinHeight, ' - ') + 3, 3);
        $partnerMaxHeight = substr($partnerPref->partnerMaxHeight, strpos($partnerPref->partnerMaxHeight, ' - ') + 3, 3);
        $users->whereBetween(DB::raw($height), [DB::raw('SUBSTRING(SUBSTRING_INDEX(user_partner_prefs.partnerMinHeight, " - ", -1), 1, 3)'), DB::raw('SUBSTRING(SUBSTRING_INDEX(user_partner_prefs.partnerMaxHeight, " - ", -1), 1, 3)')]);
        $users->whereBetween(DB::raw('SUBSTRING(SUBSTRING_INDEX(user_details.height, " - ", -1), 1, 3)'), [$partnerMinHeight, $partnerMaxHeight]);

        // maritalStatus
        $users->where(function (Builder $query) use ($user, $detail) {
            $query
                ->whereJsonContains('user_partner_prefs.partnerMaritalStatus', $detail->maritalStatus)
                ->orWhere('user_partner_prefs.partnerMaritalStatus', '[]')
                ->orWhereNull('user_partner_prefs.partnerMaritalStatus');
        });

        if(!empty($partnerPref->partnerMaritalStatus)) {
            $users->whereIn('user_details.maritalStatus', $partnerPref->partnerMaritalStatus);
        }

        // motherTongue
        $users->where(function (Builder $query) use ($user) {
            $query
                ->whereJsonContains('user_partner_prefs.partnerMotherTongue', $user->motherTongue)
                ->orWhere('user_partner_prefs.partnerMotherTongue', '[]')
                ->orWhereNull('user_partner_prefs.partnerMotherTongue');
        });

        if(!empty($partnerPref->partnerMotherTongue)) {
            $users->whereIn('users.motherTongue', $partnerPref->partnerMotherTongue);
        }

        // religion
        //$users->where(function (Builder $query) use ($user) {
        //    $query
                //->whereJsonContains('user_partner_prefs.partnerReligion', $user->religion)
				//->where('user_partner_prefs.partnerReligion', $user->religion)
		//		->where('users.religion', $user->religion);
                //->orWhere('user_partner_prefs.partnerReligion', '[]')
        //        ->orWhereNull('user_partner_prefs.partnerReligion');
        //});
		
        if(!empty($partnerPref->partnerReligion)) {
            $users->where('users.religion', $partnerPref->partnerReligion);
        }

        // bodyType
        //$users->where(function (Builder $query) use ($partnerPref) {
        //    $query
        //        ->whereJsonContains('user_partner_prefs.partnerBodyType', $partnerPref->partnerBodyType)
        //        ->orWhere('user_partner_prefs.partnerBodyType', '[]')
        //        ->orWhereNull('user_partner_prefs.partnerBodyType');
        //});
		
		if(!empty($partnerPref->partnerBodyType)) {
            $users->whereIn('user_details.bodyType', $partnerPref->partnerBodyType);
        }

        // skinColor
        //$users->where(function (Builder $query) use ($detail) {
        //    $query
        //        ->whereJsonContains('user_partner_prefs.partnerSkinColor', $detail->skinColor)
        //        ->orWhere('user_partner_prefs.partnerSkinColor', '[]')
        //        ->orWhereNull('user_partner_prefs.partnerSkinColor');
        //});

        if(!empty($partnerPref->partnerSkinColor)) {
            $users->whereIn('user_details.skinColor', $partnerPref->partnerSkinColor);
        }

        // eatingHabits
        //$users->where(function (Builder $query) use ($detail) {
        //    $query
        //        ->whereJsonContains('user_partner_prefs.partnerEatingHabits', $detail->eatingHabits)
        //        ->orWhere('user_partner_prefs.partnerEatingHabits', '[]')
        //        ->orWhereNull('user_partner_prefs.partnerEatingHabits');
        //});

        if(!empty($partnerPref->partnerEatingHabits)) {
            $users->whereIn('user_details.eatingHabits', $partnerPref->partnerEatingHabits);
        }

        // drinking
        //$users->where(function (Builder $query) use ($detail) {
        //    $query
        //        ->whereJsonContains('user_partner_prefs.partnerDrinking', $detail->drinking)
        //        ->orWhere('user_partner_prefs.partnerDrinking', '[]')
        //        ->orWhereNull('user_partner_prefs.partnerDrinking');
        //});

        if(!empty($partnerPref->partnerDrinking)) {
            $users->whereIn('user_details.drinking', $partnerPref->partnerDrinking);
        }

        // smoking
        //$users->where(function (Builder $query) use ($detail) {
        //    $query
        //        ->whereJsonContains('user_partner_prefs.partnerSmoking', $detail->smoking)
        //        ->orWhere('user_partner_prefs.partnerSmoking', '[]')
        //        ->orWhereNull('user_partner_prefs.partnerSmoking');
        //});

        if(!empty($partnerPref->partnerSmoking)) {
            $users->whereIn('user_details.smoking', $partnerPref->partnerSmoking);
        }

        // fluentLanguage
        //$users->where(function (Builder $query) use ($detail) {
        //    $query
        //        ->whereJsonContains('user_partner_prefs.partnerFluentIn', $detail->fluentLanguage)
        //        ->orWhere('user_partner_prefs.partnerFluentIn', '[]')
        //        ->orWhereNull('user_partner_prefs.partnerFluentIn');
        //});

        if(!empty($partnerPref->partnerFluentIn)) {
            $users->whereIn('user_details.fluentLanguage', $partnerPref->partnerFluentIn);
        }

        // No of children
        //if($detail->maritalStatus != User::MARITAL_STATUS_UNMARRIED) {
        //    $users->where(function (Builder $query) use ($detail) {
        //        $query
        //            ->where('user_partner_prefs.partnerNoOfChildren', '>=', $detail->numOfChildren)
        //            ->orWhereNull('user_partner_prefs.partnerNoOfChildren');
        //    });
        //}

        if(!empty($partnerPref->partnerNoOfChildren)) {
            $users->where(function (Builder $query) use ($partnerPref) {
                $query->where(function (Builder $query) use ($partnerPref) {
                    $query
                        ->where('user_details.maritalStatus', '!=', User::MARITAL_STATUS_UNMARRIED)
                        ->where('user_details.numOfChildren', '<=', $partnerPref->partnerNoOfChildren);
                })
                ->orWhere('user_details.maritalStatus', User::MARITAL_STATUS_UNMARRIED);
            });
        }

        // Children living with me
        //if($detail->numOfChildren > 0) {
        //    $users->where(function (Builder $query) use ($detail) {
        //        $query
        //            ->where('user_partner_prefs.partnerChildrenLivingWithPartner', $detail->childLiveWithMe)
        //            ->orWhere('user_partner_prefs.partnerChildrenLivingWithPartner', 1)
        //            ->orWhereNull('user_partner_prefs.partnerChildrenLivingWithPartner');
        //    });
        //}

        if($partnerPref->partnerChildrenLivingWithPartner == 0) {
            $users->where(function (Builder $query) use ($partnerPref) {
                $query
                    ->where(function (Builder $query) use ($partnerPref) {
                        $query
                            ->where('user_details.numOfChildren', '>', 0)
                            ->where('user_details.childLiveWithMe', 0);
                    })
                    ->orWhere('user_details.numOfChildren', 0);
            });
        }

        // Star
        //if($user->religion == User::RELIGION_HINDU) {
        //    $users->where(function (Builder $query) use ($detail) {
        //        $query
        //            ->whereJsonContains('user_partner_prefs.partnerStar', $detail->star)
        //            ->orWhere('user_partner_prefs.partnerStar', '[]')
        //            ->orWhereNull('user_partner_prefs.partnerStar');
        //    });
        //}

        if(!empty($partnerPref->partnerStar)) {
            $users->where(function (Builder $query) use ($partnerPref) {
                $query
                    ->where(function (Builder $query) use ($partnerPref) {
                        $query
                            ->where('users.religion', User::RELIGION_HINDU)
                            ->whereIn('user_details.star', $partnerPref->partnerStar);
                    })
                    ->orWhere('users.religion', '!=', User::RELIGION_HINDU);
            });
        }

        // Country
        //$users->where(function (Builder $query) use ($detail) {
        //    $query
//                ->whereJsonContains('user_partner_prefs.partnerLocationCountry', $user->currentCountry)
        //        ->where('user_partner_prefs.partnerLocationCountry', $detail->currentCountry)
        //        ->orWhereNull('user_partner_prefs.partnerLocationCountry');
        //});

        if(!empty($partnerPref->partnerLocationCountry)) {
//            $users->whereIn('user_details.currentCountry', $partnerPref->partnerLocationCountry);
            $users->where('user_details.currentCountry', $partnerPref->partnerLocationCountry);
        }

        // State
        //if($user->currentCountry == User::COUNTRY_INDIA) {
        //    $users->where(function (Builder $query) use ($detail) {
        //        $query
        //            ->where('user_partner_prefs.partnerLocationState', $detail->currentState)
        //            ->orWhereNull('user_partner_prefs.partnerLocationState');
        //    });
        //}

        if(!empty($partnerPref->partnerLocationState)) {
            $users->where(function (Builder $query) use ($partnerPref) {
                $query
                    ->where(function (Builder $query) use ($partnerPref) {
                        $query
                            ->where('user_details.currentCountry', User::COUNTRY_INDIA)
                            ->where('user_details.currentState', $partnerPref->partnerLocationState);
                    })
                    ->orWhere('user_details.currentCountry', '!=', User::COUNTRY_INDIA);
            });
        }

        // City
        if($user->currentCountry == User::COUNTRY_INDIA) {
            $users->where(function (Builder $query) use ($detail) {
                $query
                    ->where('user_partner_prefs.partnerLocationCity', $detail->currentCity)
                    ->orWhereNull('user_partner_prefs.partnerLocationCity');
            });
        }

        if(!empty($partnerPref->partnerLocationCity)) {
            $users->where(function (Builder $query) use ($partnerPref) {
                $query
                    ->where(function (Builder $query) use ($partnerPref) {
                        $query
                            ->where('user_details.currentCountry', User::COUNTRY_INDIA)
                            ->where('user_details.currentCity', $partnerPref->partnerLocationCity);
                    })
                    ->orWhere('user_details.currentCountry', '!=', User::COUNTRY_INDIA);
            });
        }

        /*$users = $users->get();

        return $users;*/

        $users = $users->simplePaginate($pageSize);

        return $this->success('Fetched matched users', $users);
    }
}
