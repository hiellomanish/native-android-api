<?php

namespace App\Http\Controllers\API;

use App\Model\FUserDetail;
use App\Model\MUserDetail;
use App\Model\User;
use App\Model\UserDetail;
use App\Model\UserPartnerPref;
use App\Notifications\ProfileVisits;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public function profile() {
        /** @var User $user */
        $user = Auth::user();
        $user->mainPhotoLink = url('storage/photos/' . $user->mainPhotoLink);

        $detail = $user->detail;
        $user->accountStatus = $detail->accountStatus;

        $user = $user->makeHidden('detail')->toArray();

        return $this->success("Logged user", $user);
    }

    public function profileDetail($id) {
        $own = $id == 'me';
        $id = $own ? Auth::id() : $id;
        $lockedPhotoUrl = config('constants.locked_photo_url');

        /** @var User $user */
        $user = User::find($id);

        if(!$own) {
            $user->comm;
            $user->commAlt;
        }

        if(is_null($user)) {
            return $this->error('User not found', 404);
        }

        $detailClass = $user->detailClass();
        /** @var MUserDetail|FUserDetail $detail */
        $detail = $detailClass::where('userId', $id)->first();

        if(is_null($detail)) {
            return $this->error('User detail not found', 404);
        }

        unset($detail->id);
        $detail->imageLinks = collect($detail->imageLinks)->map(function ($item, $key) {
            return (Str::startsWith($item, 'http://') || Str::startsWith($item, 'https://')) ? $item : url('storage/photos/'.$item);
        });

        /** @var UserPartnerPref $partnerPref */
        $partnerPref = UserPartnerPref::where('userId', $id)->first();

        if(is_null($partnerPref)) {
            return $this->error('User partner pref not found', 404);
        }

        unset($partnerPref->id);

        $userDetail = array_merge($user->toArray(), $detail->toArray(), $partnerPref->toArray());

        return $this->success('User Fetched', $userDetail);
    }

    public function updateProfile(Request $request) {
        $id = Auth::id();

        $validator = Validator::make($request->all(), [
            'firstName' => 'required',
            'lastName' => 'required',
			'ISDCode' => 'required',
            'mobileNo' => 'required|unique:users,mobileNo,'.$id,
            'email' => 'required|unique:users,email,'.$id,
//            'gender' => 'required|in:Male,Female',
            'location' => 'required',
            'religion' => 'required',
            'motherTongue' => 'required',
//            'password' => 'required',
//            'password_confirmation' => 'confirmed',

            //Personal
            'aboutMe' => 'required',
//            'firstName' => 'required',
//            'lastName' => 'required',
            'birthDate' => 'required|date',
            'height' => 'required',
            'maritalStatus' => 'required',
//            'numOfChildren' => 'required_if:maritalStatus,'.User::MARITAL_STATUS_DIVORCEE,
            'numOfChildren' => [Rule::requiredIf(function() use ($request) {
                return $request->maritalStatus  != User::MARITAL_STATUS_UNMARRIED;
            })],
            'childLiveWithMe' => [Rule::requiredIf(function() use ($request) {
                return $request->maritalStatus  != User::MARITAL_STATUS_UNMARRIED && $request->numOfChildren > 0;
            })],
            'bodyType' => 'required',
            'skinColor' => 'required',
            'physicalDisabilities' => 'required',
//            'motherTongue' => 'required',
//            'religion' => 'required',
            'caste' => 'required',
            'subCaste' => 'required',
            'star' => 'required_if:religion,Hindu',
            'currentCountry' => 'required',
            'currentState' => 'required_if:currentCountry,India',
//            'currentDistrict' => 'required_if:currentCountry,India', // TODO: uncomment it
//            'currentCity' => 'required_if:currentCountry,India', // TODO: uncomment it
            'eatingHabits' => 'required',
            'drinking' => 'required',
            'smoking' => 'required',

            // Professional and Family
            //  Professional
            'educationLevel' => 'required',
            'graduateCourse' => [Rule::requiredIf(function() use ($request) {
                return array_search(trim($request->educationLevel), [User::EDUCATION_LEVEL_GRADUATE, User::EDUCATION_LEVEL_MASTERS, User::EDUCATION_LEVEL_PHD]) !== false;
            })],
            'graduationSubject' => [Rule::requiredIf(function() use ($request) {
                return array_search(trim($request->educationLevel), [User::EDUCATION_LEVEL_GRADUATE, User::EDUCATION_LEVEL_MASTERS, User::EDUCATION_LEVEL_PHD]) !== false;
            })],
			'masterCourse' => [Rule::requiredIf(function() use ($request) {
                return array_search($request->educationLevel, [User::EDUCATION_LEVEL_MASTERS, User::EDUCATION_LEVEL_PHD]) !== false;
            })],
			'masterSubject' => [Rule::requiredIf(function() use ($request) {
                return array_search($request->educationLevel, [User::EDUCATION_LEVEL_MASTERS, User::EDUCATION_LEVEL_PHD]) !== false;
            })],
			'doctorateCourse' => [Rule::requiredIf(function() use ($request) {
                return array_search($request->educationLevel, [User::EDUCATION_LEVEL_PHD]) !== false;
            })],
			'doctorateSubject' => [Rule::requiredIf(function() use ($request) {
                return array_search($request->educationLevel, [User::EDUCATION_LEVEL_PHD]) !== false;
            })],
            'profession' => 'required',
			'jobFamily' => 'required',
            'salary' => 'required',
            //  Family
//            'aboutMyFamily',
//            'familyName',
            'familyValue' => 'required',
            'familyType' => 'required',
            'financialStatus' => 'required',
            'candidatesShare' => 'required',
            'fatherName' => 'required',
            'fatherStatus' => 'required',
            'motherName' => 'required',
            'motherStatus' => 'required',
            'familyCountry' => 'required',
            'familyState' => 'required_if:familyCountry,India',
//            'familyDistrict' => 'required_if:familyCountry,India', // TODO: uncomment it
//            'familyCity' => 'required_if:familyCountry,India', // TODO: uncomment it
            'noOfBrothers' => 'required',
            'noOfBrothersMarried' => [Rule::requiredIf(function() use ($request) {
                return $request->noOfBrothers > 0;
            })],
            'noOfSisters' => 'required',
            'noOfSistersMarried' => [Rule::requiredIf(function() use ($request) {
                return $request->noOfSisters > 0;
            })],

            // Contact Info
//            'mobileNo',
//            'email',
            'parentLandlineNo' => 'required_without_all:fatherMobileNo,motherMobileNo', // missing
            'fatherMobileNo' => 'required_without_all:parentLandlineNo,motherMobileNo', // missing, but found `parentMobileNo`
            'motherMobileNo' => 'required_without_all:fatherMobileNo,parentLandlineNo', // missing, but found `parentMobileNo`
        ]);

        /*$validator->sometimes('childLiveWithMe', 'required|numeric', function ($input) {
            return $input->numOfChildren > 0;
        });*/

        if ($validator->fails()) {
            return $this->error('Some parameters are missing or invalid', Response::HTTP_BAD_REQUEST, $validator->errors());
        }

        DB::beginTransaction();

        /** @var User $user */
        $user = Auth::user();
        $user->fill($request->only([
            'firstName',
            'lastName',
            'gender',
			'ISDCode',
            'mobileNo',
            'email',
            'religion',
            'motherTongue',
            'location'
        ]));
        $user->save();

        $detail = $user->detail;
        $detail->birthDate = date('Y-m-d', strtotime($request->get('birthDate')));
        $detail->fill($request->only([
            'aboutMe',
//            'firstName',
//            'lastName',
//            'birthDate',
            'height',
            'weight',
            'maritalStatus',
            'numOfChildren',
            'childLiveWithMe',
            'bodyType',
            'skinColor',
            'physicalDisabilities',
//            'motherTongue',
//            'religion',
            'caste', // missing
            'subCaste', // missing
            'star', // missing
            'currentCountry',
			'currentState',
			'currentDistrict', // missing
			'currentTaluk',
			'currentCity',
            'eatingHabits',  // missing
            'drinking',
            'smoking',
            'hobbies',
            'fluentLanguage',
            // Professional
            'educationLevel',
			'graduateCourse',
            'graduationSubject',
            'masterCourse',
			'masterSubject',
			'doctorateCourse',
			'doctorateSubject',
            'premierInstitution',
            'profession',
			'jobFamily',
			'job',
            'salary',
            //Family
            'aboutMyFamily',
            'familyName',
            'familyValue',
            'familyType',
            'financialStatus',
            'candidatesShare',  // missing
            'fatherName',
            'fatherStatus',
            'motherName',
            'motherStatus',
            'familyCountry',
            'familyState',
            'familyDistrict', // missing
			'familyTaluk',
            'familyCity',
            'noOfBrothers',
            'noOfBrothersMarried',
            'noOfSisters',
            'noOfSistersMarried',
            //Contact
			'landLineISD',
            'parentLandlineNo',
            'fatherISD',
			'fatherMobileNo',
            'motherISD',
			'motherMobileNo',
        ]));
        $detail->save();

        /** @var UserPartn                                                                                                                                                                                                                                                                                                                                                                                                                                                                erPref $partnerPref */
        $partnerPref = $user->partnerPref;
        $partnerPref->fill($request->only([
            'aboutMyPartner',
            'partnerMinAge',
            'partnerMaxAge',
            'partnerMinHeight',
            'partnerMaxHeight',
            'partnerMaritalStatus',
            'partnerNoOfChildren',
            'partnerChildrenLivingWithPartner',
			'partnerEducation',
			'partnerJob',
			'partnerSalary',
			'partnerProfession',
			'partnerGraduateSubject',
			'partnerMastersSubject',
			'partnerFinancialStatus',
			'partnerCandidateShare',
            'partnerBodyType', // missing
            'partnerSkinColor', // missing
            'partnerPhysicalDisabilities', // missing
            'partnerMotherTongue',
            'partnerReligion',
			'partnerCasteAndSubCaste',
            'partnerStar', // missing
            'partnerLocationCountry',
            'partnerLocationState',
//            'partnerLocationDistrict', // missing
            'partnerLocationCity',
            'partnerEatingHabits', // missing, but found `partnerDiet`
            'partnerDrinking',
            'partnerSmoking',
            'partnerFluentIn' // missing
        ]));
        $partnerPref->save();

        DB::commit();

        return $this->success("You have successfully updated your profile", null);
    }

    public function getPhotos($id) {
        $own = $id == 'me';
        $id = $own ? Auth::id() : $id;

        $user = User::find($id);

        if(is_null($user)) {
            return $this->error('User not found', 404);
        }

        /** @var MUserDetail|FUserDetail $detail */
        $detail = $user->detail;
        unset($user->detail);

        if(is_null($detail)) {
            return $this->error('User detail not found', 404);
        }

        $detail->imageLinks = collect($detail->imageLinks)->map(function ($item, $key) {
            return (Str::startsWith($item, 'http://') || Str::startsWith($item, 'https://')) ? $item : url('storage/photos/'.$item);
        });

        return $this->success('Images fetched', $detail->imageLinks);
    }

    public function uploadPhotos(Request $request) {
        info("files", $_FILES);

        if($request->has('photos')) {
            $photos = $request->file('photos');

            if(is_array($photos)) {
                $uploadedPhotos = [];

                /** @var UploadedFile $photo */
                foreach($photos as $photo) {
                    $photoName = Auth::id() . '_' . $photo->getClientOriginalName();
                    $result = $photo->storeAs('photos', $photoName);

                    if($result) {
                        $uploadedPhotos[] = $photoName;
                    }
                }

                /** @var User $user */
                $user = Auth::user();

                /** @var FUserDetail|MUserDetail $detail */
                $detail = $user->detail;

                $imageLinks = [];
                if(!empty($detail->imageLinks)) {
                    $imageLinks = $detail->imageLinks;
                }

                $imageLinks = array_merge($imageLinks, $uploadedPhotos);

                $detail->imageLinks = $imageLinks;
                $detail->save();

                if(empty($user->mainPhotoLink) && !empty($imageLinks)) {
                    $user->mainPhotoLink = $imageLinks[0];
                    $user->save();
                }

                $imageLinks = collect($imageLinks)->map(function ($item, $key) {
                    return (Str::startsWith($item, 'http://') || Str::startsWith($item, 'https://')) ? $item : url('storage/photos/'.$item);
                });

                return $this->success('Photo upload successfully', $imageLinks);
            }
        }

         return $this->error('Error while upload photo');
    }

    public function deletePhotos(Request $request) {
        /** @var User $user */
        $user = Auth::user();

        if(is_null($user)) {
            return $this->error('User not found', 404);
        }

        /** @var MUserDetail|FUserDetail $detail */
        $detail = $user->detail;
        unset($user->detail);

        if(is_null($detail)) {
            return $this->error('User detail not found', 404);
        }

        $photoToDelete = $request->get('photo');

        $imageLinks = collect($detail->imageLinks)->filter(function ($value, $key) use ($photoToDelete) {
            $value = (Str::startsWith($value, 'http://') || Str::startsWith($value, 'https://')) ? $value : url('storage/photos/'.$value);
            return $value != $photoToDelete;
        })->values();

        $detail->imageLinks = $imageLinks;
        $detail->save();

        $mainPhotoLink = url('storage/photos/'.$user->mainPhotoLink);
        if((empty($user->mainPhotoLink) || $mainPhotoLink == $photoToDelete) && !empty($imageLinks)) {
            $user->mainPhotoLink = $imageLinks[0];
            $user->save();
        }

        $imageLinks = collect($imageLinks)->map(function ($item, $key) {
            return (Str::startsWith($item, 'http://') || Str::startsWith($item, 'https://')) ? $item : url('storage/photos/'.$item);
        });

        return $this->success('Photo removed', $imageLinks);
    }

}

