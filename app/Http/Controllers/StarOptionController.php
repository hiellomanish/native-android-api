<?php

namespace App\Http\Controllers;

use App\Model\StarOption;
use Illuminate\Http\Request;

class StarOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\StarOption  $starOption
     * @return \Illuminate\Http\Response
     */
    public function show(StarOption $starOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\StarOption  $starOption
     * @return \Illuminate\Http\Response
     */
    public function edit(StarOption $starOption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\StarOption  $starOption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StarOption $starOption)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\StarOption  $starOption
     * @return \Illuminate\Http\Response
     */
    public function destroy(StarOption $starOption)
    {
        //
    }
}
