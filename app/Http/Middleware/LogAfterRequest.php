<?php

namespace App\Http\Middleware;

use App\ApiLog;
use Closure;
use Illuminate\Http\Request;

class LogAfterRequest {

    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate(Request $request, $response)
    {
        info('ApiLog done===========================');
        info('URL: '.$request->fullUrl());
        info('Method: '.$request->getMethod());
        info('IP Address: '.$request->getClientIp());
        info('Status Code: '.$response->status());
        info('Content: '.$response->content());
    }

}