<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailOTP extends Mailable
{
    use Queueable, SerializesModels;

    var $name;
    var $otp;

    /**
     * Create a new message instance.
     * @param $name
     * @param $otp
     */
    public function __construct($name, $otp)
    {
        $this->name = $name;
        $this->otp = $otp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Please verify your email address')
            ->view('emails.mail-otp');
    }
}
