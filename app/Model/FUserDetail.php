<?php

namespace App\Model;

class FUserDetail extends UserDetail
{
    const TABLE_NAME = 'user_details_f';
    protected $table = FUserDetail::TABLE_NAME;
    protected $guarded = [];
}
