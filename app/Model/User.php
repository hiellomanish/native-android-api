<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const MARITAL_STATUS_UNMARRIED = 'Unmarried';

    const EDUCATION_LEVEL_HIGH_SCHOOL = 'high school or less';
    const EDUCATION_LEVEL_10TH_SECONDARY = '10th/secondary';
    const EDUCATION_LEVEL_12TH_HIGHER_SECONDARY = 'plus two/higher secondary';
    const EDUCATION_LEVEL_GRADUATE = 'diploma/graduation/pofessional certifications';
    const EDUCATION_LEVEL_MASTERS = 'post graduation';
    const EDUCATION_LEVEL_PHD = 'phd';

    const RELIGION_HINDU = 'Hindu';

    const COUNTRY_INDIA = 'India';

    public static $commClass = MComm::class;
    public static $commAltClass = FComm::class;

    protected $table = 'users';
    protected $guarded = ['id'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'verified' => 'boolean',
    ];

    public function findForPassport($identifier)
    {
        return $this->orWhere('email', $identifier)->orWhere('mobileNo', $identifier)->first();
    }

    public function detail() {
        $detailClass = $this->isMale() ? MUserDetail::class : FUserDetail::class;
        return $this->hasOne($detailClass, 'userId');
    }

    public function partnerPref() {
        return $this->hasOne(UserPartnerPref::class, 'userId');
    }

    public function comm() {
        if(empty($this->id)) {
            $detailClass = self::$commClass;
        } else {
            $detailClass = $this->isMale() ? MComm::class : FComm::class;
        }

        return $this->hasOne($detailClass, 'userId1')->where('userId2', Auth::id());
    }

    public function commAlt() {
        if(empty($this->id)) {
            $detailClass = self::$commAltClass;
        } else {
            $detailClass = $this->isMale() ? FComm::class : MComm::class;
        }

        return $this->hasOne($detailClass, 'userId2')->where('userId1', Auth::id());
    }

    public function isMale() {
        return $this->gender == 'male';
    }

    public function isFemale() {
        return $this->gender == 'female';
    }

    public function commClass() {
        return $this->isMale() ? MComm::class : FComm::class;
    }

    public function commTable() {
        $class = $this->commClass();
//        return (new $commClass())->tableName();
        return $class::TABLE_NAME;
    }

    public function commAltClass() {
        return $this->isFemale() ? MComm::class : FComm::class;
    }

    public function commAltTable() {
        $class = $this->commAltClass();
        return $class::TABLE_NAME;
    }

    public function detailClass() {
        return $this->isMale() ? MUserDetail::class : FUserDetail::class;
    }

    public function detailTable() {
        $class = $this->detailClass();
//        return (new $commClass())->tableName();
        return $class::TABLE_NAME;
    }

    public function detailAltClass() {
        return $this->isFemale() ? MUserDetail::class : FUserDetail::class;
    }

    public function detailAltTable() {
        $class = $this->detailAltClass();
//        return (new $commClass())->tableName();
        return $class::TABLE_NAME;
    }

    public function fcmRegistrationIds() {
        return $this->hasMany(FcmRegistrationId::class, 'user_id');
    }

    /**
     * Route notifications for the fcm channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForFcm($notification)
    {
        /** @var Collection $fcmRegistrationIds */
        $fcmRegistrationIds = $this->fcmRegistrationIds;
        return $fcmRegistrationIds = $fcmRegistrationIds->pluck('registration_id')->toArray();
    }
}
