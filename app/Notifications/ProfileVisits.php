<?php

namespace App\Notifications;

use App\Model\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;

class ProfileVisits extends Notification
{
    use Queueable;

    /** @var  User $user */
    var $user;

    /**
     * Create a new notification instance.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FcmChannel::class];
    }

    public function toFcm($notifiable)
    {
        // The FcmNotification holds the notification parameters
        /*$fcmNotification = FcmNotification::create()
            ->setTitle('Your account has been activated')
            ->setBody('Thank you for activating your account.');*/

        // The FcmMessage contains other options for the notification
        return FcmMessage::create()
            ->setPriority(FcmMessage::PRIORITY_HIGH)
            ->setTimeToLive(86400)
            ->setData((object) [ 'message' => "{$this->user->firstName} {$this->user->lastName} visits your profile" ]);
//            ->setNotification($fcmNotification);
    }
}
