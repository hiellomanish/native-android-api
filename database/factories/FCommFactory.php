<?php

use Faker\Generator as Faker;

$factory->define(\App\Model\FComm::class, function (Faker $faker) {
    $imStatus = ['Unread', 'Pending', 'Accepted', 'Rejected'];
    $prStatus = ['Unread', 'Pending', 'Approved', 'Rejected'];

    return [
        'contact_status' => 0,
        'profileVisits' => $profileVisits = rand(0, 1),
        'profileVisitsDate' => $profileVisits == 1 ? $faker->dateTime : null,
        'interestMessage' => $interestMessage = rand(0, 1),
        'interestMessageDate' => $interestMessage == 1 ? $faker->dateTime : null,
        'imStatus' => $interestMessage == 1 ? $imStatus[rand(0, 3)] : 'Unread',
        'imStatusDate' => $interestMessage == 1 ? $faker->dateTime : null,
        'photoRequest' => $photoRequest = rand(0, 1),
        'photoRequestDate' => $photoRequest == 1 ? $faker->dateTime : null,
        'prStatus' => $photoRequest == 1 ? $prStatus[rand(0, 3)] : 'Unread',
        'prStatusDate' => $photoRequest == 1 ? $faker->dateTime : null,
        'contactRequest' => $contactRequest = rand(0, 1),
        'contactRequestDate' => $contactRequest == 1 ? $faker->dateTime : null,
        'crStatus' => $contactRequest == 1 ? $prStatus[rand(0, 3)] : 'Unread',
        'crStatusDate' => $contactRequest == 1 ? $faker->dateTime : null,
    ];
});
