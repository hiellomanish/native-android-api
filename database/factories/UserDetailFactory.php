<?php

use Faker\Generator as Faker;

$factory->define(\App\Model\MUserDetail::class, function (Faker $faker) {
    return [
        'birthDate' => $faker->date('Y-m-d', '1996-12-31'),
        'height' => "5ft 5in - 165cm",
        'weight' => 80,
        'skinColor' => '"Very Fair"',
        'bodyType' => "Slim",
        'physicalDisabilities' => 0,
        'maritalStatus' => '"Unmarried"',
        'numOfChildren' => 0,
        'childLiveWithMe' => 0,
        'fluentLanguage' => "English",
        'eatingHabits' => "Vegetarian",
        'drinking' => "No",
        'smoking' => "Occasionally",
        'aboutMe' => $faker->sentence,
        'educationLevel' => "high school",
        'graduationSubject' => "Aviation Degree",
        'masterSubject' => "M.F.M. (Financial Management)",
        'profession' => "Agriculture & Farming Professional",
        'salary' => "10 Lakh To 15 Lakh",
        'currentCity' => "Kozhikkode",
        'currentState' => "Kerala",
        'currentCountry' => "India",
        //Family
        'aboutMyFamily' => $faker->sentence,
        'familyName' => $faker->lastName,
        'familyValue' => '"Moderate"',
        'familyType' => '"Joint"',
        'financialStatus' => '"Middle class (1 to 5 crore)"',
        'candidatesShare' => $faker->word,  // missing
        'fatherName' => $faker->firstNameMale,
        'fatherStatus' => '"Business"',
        'motherName' => $faker->firstNameFemale,
        'motherStatus' => '"Not working"',
        'familyCountry' => "India",
        'familyState' => "Kerala",
        'familyCity' => "Kozhikkode",
        'noOfBrothers' => $noOfBrothers = rand(0, 6),
        'noOfBrothersMarried' => rand(0, $noOfBrothers),
        'noOfSisters' => $noOfSisters = rand(0, 6),
        'noOfSistersMarried' => rand(0, $noOfSisters),
        'accountStatus' => rand(0, 2),
        //Contact
        'parentLandlineNo' => $faker->phoneNumber,
        'fatherMobileNo' => $faker->phoneNumber,
        'motherMobileNo' => $faker->phoneNumber
    ];
});
