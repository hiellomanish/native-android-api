<?php

use Faker\Generator as Faker;

$factory->define(\App\Model\UserPartnerPref::class, function (Faker $faker) {
    return [
        'aboutMyPartner' => $faker->sentence
    ];
});
