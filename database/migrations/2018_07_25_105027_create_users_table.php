<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstName');
            $table->string('lastName');
            $table->enum('gender', ['male', 'female']);
			$table->string('ISDCode');
            $table->string('mobileNo')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('photoLock')->default(false);
            $table->text('mainPhotoLink')->nullable();
            $table->string('religion');
            $table->string('motherTongue');
            $table->string('location');
            $table->rememberToken()->nullable();
            $table->boolean('verified')->default(false);
            $table->timestamp('lastLoginAt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

