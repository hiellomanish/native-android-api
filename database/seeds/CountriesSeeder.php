<?php

use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->countries() as $key => $value) {
            DB::table('countries')->insert([
                'id' => $value[0],
                'countryCode' => $value[1],
				'countries' => $value[2],
				'countryCodeNo' => $value[3],
            ]);
        }
    }


    private function countries()
    {
        $countries = [
			[1, 'IN', 'India','91'],
			[2, 'US', 'United States of America','1'],
			[3, 'AE', 'United Arab Emirates','971'],
			[4, 'GB', 'United Kingdom','44'],
			[5, 'AU', 'Australia','61'],
			[6, 'SG', 'Singapore','65'],
			[7, 'CA', 'Canada','1'],
			[8, 'QA', 'Qatar','974'],
			[9, 'KW', 'Kuwait','965'],
			[10, 'OM', 'Oman','968'],
			[11, 'BH', 'Bahrain','973'],
			[12, 'SA', 'Saudi Arabia','966'],
			[13, 'MY', 'Malaysia','60'],
			[14, 'DE', 'Germany','49'],
			[15, 'NZ', 'New Zealand','64'],
			[16, 'FR', 'France','33'],
			[17, 'IE', 'Ireland','353'],
			[18, 'CH', 'Switzerland','41'],
			[19, 'ZA', 'South Africa','27'],
			[20, 'LK', 'Sri Lanka','94'],
			[21, 'ID', 'Indonesia','62'],
			[22, 'NP', 'Nepal','977'],
			[23, 'PK', 'Pakistan','92'],
			[24, 'BD', 'Bangladesh','880'],
			[25, 'AF', 'Afghanistan','93'],
			[26, 'AL', 'Albania','355'],
			[27, 'DZ', 'Algeria','213'],
			[28, 'AS', 'American Samoa','1-684'],
			[29, 'AD', 'Andorra','376'],
			[30, 'AO', 'Angola','244'],
			[31, 'AI', 'Anguilla','1-264'],
			[32, 'AQ', 'Antarctica','672'],
			[33, 'AG', 'Antigua And Barbuda','1-268'],
			[34, 'AR', 'Argentina','54'],
			[35, 'AM', 'Armenia','374'],
			[36, 'AW', 'Aruba','297'],
			[37, 'AT', 'Austria','43'],
			[38, 'AZ', 'Azerbaijan','994'],
			[39, 'BS', 'Bahamas','1-242'],
			[40, 'BB', 'Barbados','1-246'],
			[41, 'BY', 'Belarus','375'],
			[42, 'BE', 'Belgium','32'],
			[43, 'BZ', 'Belize','501'],
			[44, 'BJ', 'Benin','229'],
			[45, 'BM', 'Bermuda','1-441'],
			[46, 'BT', 'Bhutan','975'],
			[47, 'BO', 'Bolivia','591'],
			[48, 'BA', 'Bosnia and Herzegovina','387'],
			[49, 'BW', 'Botswana','267'],
			[50, 'BV', 'Bouvet Island','47'],
			[51, 'BR', 'Brazil','55'],
			[52, 'IO', 'British Indian Ocean Territory','246'],
			[53, 'BN', 'Brunei','673'],
			[54, 'BG', 'Bulgaria','359'],
			[55, 'BF', 'Burkina Faso','226'],
			[56, 'BI', 'Burundi','257'],
			[57, 'KH', 'Cambodia','855'],
			[58, 'CM', 'Cameroon','237'],
			[59, 'CV', 'Cape Verde','238'],
			[60, 'KY', 'Cayman Islands','1-345'],
			[61, 'CF', 'Central African Republic','236'],
			[62, 'TD', 'Chad','235'],
			[63, 'CL', 'Chile','56'],
			[64, 'CN', 'China','86'],
			[65, 'CX', 'Christmas Island','61'],
			[66, 'CC', 'Cocos (Keeling) Islands','61'],
			[67, 'CO', 'Colombia','57'],
			[68, 'KM', 'Comoros','269'],
			[69, 'CG', 'Congo (Brazzaville)','242'],
			[70, 'CD', 'Congo (Kinshasa)','243'],
			[71, 'CK', 'Cook Islands','682'],
			[72, 'CR', 'Costa Rica','506'],
			[73, 'CI', 'Cote D\'Ivoire (Ivory Coast)','225'],
			[74, 'HR', 'Croatia (Hrvatska)','385'],
			[75, 'CU', 'Cuba','53'],
			[76, 'CY', 'Cyprus','357'],
			[77, 'CZ', 'Czech Republic','420'],
			[78, 'DK', 'Denmark','45'],
			[79, 'DJ', 'Djibouti','253'],
			[80, 'DM', 'Dominica','1-767'],
			[81, 'DO', 'Dominican Republic','1-809,1-829,1-849'],
			[82, 'TP', 'East Timor',''],
			[83, 'EC', 'Ecuador','593'],
			[84, 'EG', 'Egypt','20'],
			[85, 'SV', 'El Salvador','503'],
			[86, 'GQ', 'Equatorial Guinea','240'],
			[87, 'ER', 'Eritrea','291'],
			[88, 'EE', 'Estonia','372'],
			[89, 'ET', 'Ethiopia','251'],
			[90, 'XA', 'External Territories of Australia',''],
			[91, 'FK', 'Falkland Islands','500'],
			[92, 'FO', 'Faroe Islands','298'],
			[93, 'FJ', 'Fiji','679'],
			[94, 'FI', 'Finland','358'],
			[95, 'GF', 'French Guiana','594'],
			[96, 'PF', 'French Polynesia','689'],
			[97, 'TF', 'French Southern Territories','262'],
			[98, 'GA', 'Gabon','241'],
			[99, 'GM', 'Gambia','220'],
			[100, 'GE', 'Georgia','995'],
			[101, 'GH', 'Ghana','233'],
			[102, 'GI', 'Gibraltar','350'],
			[103, 'GR', 'Greece','30'],
			[104, 'GL', 'Greenland','299'],
			[105, 'GD', 'Grenada','1-473'],
			[106, 'GP', 'Guadeloupe','590'],
			[107, 'GU', 'Guam','1-671'],
			[108, 'GT', 'Guatemala','502'],
			[109, 'XU', 'Guernsey and Alderney','44'],
			[110, 'GN', 'Guinea','224'],
			[111, 'GW', 'Guinea-Bissau','245'],
			[112, 'GY', 'Guyana','592'],
			[113, 'HT', 'Haiti','509'],
			[114, 'HM', 'Heard and McDonald Islands','672'],
			[115, 'HN', 'Honduras','504'],
			[116, 'HK', 'Hong Kong S.A.R.','852'],
			[117, 'HU', 'Hungary','36'],
			[118, 'IS', 'Iceland','354'],
			[119, 'IR', 'Iran','98'],
			[120, 'IQ', 'Iraq','964'],
			[121, 'IL', 'Israel','972'],
			[122, 'IT', 'Italy','39'],
			[123, 'JM', 'Jamaica','1-876'],
			[124, 'JP', 'Japan','81'],
			[125, 'XJ', 'Jersey','44'],
			[126, 'JO', 'Jordan','962'],
			[127, 'KZ', 'Kazakhstan','7'],
			[128, 'KE', 'Kenya','254'],
			[129, 'KI', 'Kiribati','686'],
			[130, 'KP', 'North Korea','850'],
			[131, 'KR', 'South Korea','82'],
			[132, 'KG', 'Kyrgyzstan','996'],
			[133, 'LA', 'Laos','856'],
			[134, 'LV', 'Latvia','371'],
			[135, 'LB', 'Lebanon','961'],
			[136, 'LS', 'Lesotho','266'],
			[137, 'LR', 'Liberia','231'],
			[138, 'LY', 'Libya','218'],
			[139, 'LI', 'Liechtenstein','423'],
			[140, 'LT', 'Lithuania','370'],
			[141, 'LU', 'Luxembourg','352'],
			[142, 'MO', 'Macau S.A.R.','853'],
			[143, 'MK', 'Macedonia','389'],
			[144, 'MG', 'Madagascar','261'],
			[145, 'MW', 'Malawi','265'],
			[146, 'MV', 'Maldives','960'],
			[147, 'ML', 'Mali','223'],
			[148, 'MT', 'Malta','356'],
			[149, 'XM', 'Isle of Man',''],
			[150, 'MH', 'Marshall Islands','692'],
			[151, 'MQ', 'Martinique','596'],
			[152, 'MR', 'Mauritania','222'],
			[153, 'MU', 'Mauritius','230'],
			[154, 'YT', 'Mayotte','262'],
			[155, 'MX', 'Mexico','52'],
			[156, 'FM', 'Micronesia','691'],
			[157, 'MD', 'Moldova','373'],
			[158, 'MC', 'Monaco','377'],
			[159, 'MN', 'Mongolia','976'],
			[160, 'ME', 'Montenegro','382'],
			[161, 'MS', 'Montserrat','1-664'],
			[162, 'MA', 'Morocco','212'],
			[163, 'MZ', 'Mozambique','258'],
			[164, 'MM', 'Myanmar','95'],
			[165, 'NA', 'Namibia','264'],
			[166, 'NR', 'Nauru','674'],
			[167, 'AN', 'Netherlands Antilles',''],
			[168, 'NL', 'Netherlands','31'],
			[169, 'NC', 'New Caledonia','687'],
			[170, 'NI', 'Nicaragua','505'],
			[171, 'NE', 'Niger','227'],
			[172, 'NG', 'Nigeria','234'],
			[173, 'NU', 'Niue','683'],
			[174, 'NF', 'Norfolk Island','672'],
			[175, 'MP', 'Northern Mariana Islands','1-670'],
			[176, 'NO', 'Norway','47'],
			[177, 'PW', 'Palau','680'],
			[178, 'PS', 'Palestinian Territory','970'],
			[179, 'PA', 'Panama','507'],
			[180, 'PG', 'Papua new Guinea','675'],
			[181, 'PY', 'Paraguay','595'],
			[182, 'PE', 'Peru','51'],
			[183, 'PH', 'Philippines','63'],
			[184, 'PN', 'Pitcairn Island','870'],
			[185, 'PL', 'Poland','48'],
			[186, 'PT', 'Portugal','351'],
			[187, 'PR', 'Puerto Rico','1'],
			[188, 'RE', 'Réunion','262'],
			[189, 'RO', 'Romania','40'],
			[190, 'RU', 'Russia','7'],
			[191, 'RW', 'Rwanda','250'],
			[192, 'SH', 'Saint Helena','290'],
			[193, 'KN', 'Saint Kitts And Nevis','1-869'],
			[194, 'LC', 'Saint Lucia','1-758'],
			[195, 'PM', 'Saint Pierre and Miquelon','508'],
			[196, 'VC', 'Saint Vincent And The Grenadines','1-784'],
			[197, 'WS', 'Samoa','685'],
			[198, 'SM', 'San Marino','378'],
			[199, 'ST', 'São Tomé and Príncipe','239'],
			[200, 'SN', 'Senegal','221'],
			[201, 'RS', 'Serbia','381'],
			[202, 'SC', 'Seychelles','248'],
			[203, 'SL', 'Sierra Leone','232'],
			[204, 'SK', 'Slovakia','421'],
			[205, 'SI', 'Slovenia','386'],
			[206, 'XG', 'Smaller Territories of the UK',''],
			[207, 'SB', 'Solomon Islands','677'],
			[208, 'SO', 'Somalia','252'],
			[209, 'GS', 'South Georgia','500'],
			[210, 'SS', 'South Sudan','211'],
			[211, 'ES', 'Spain','34'],
			[212, 'SD', 'Sudan','249'],
			[213, 'SR', 'Suriname','597'],
			[214, 'SJ', 'Svalbard And Jan Mayen Islands','47'],
			[215, 'SZ', 'Swaziland','268'],
			[216, 'SE', 'Sweden','46'],
			[217, 'SY', 'Syria','963'],
			[218, 'TW', 'Taiwan','886'],
			[219, 'TJ', 'Tajikistan','992'],
			[220, 'TZ', 'Tanzania','255'],
			[221, 'TH', 'Thailand','66'],
			[222, 'TG', 'Togo','228'],
			[223, 'TK', 'Tokelau','690'],
			[224, 'TO', 'Tonga','676'],
			[225, 'TT', 'Trinidad And Tobago','1-868'],
			[226, 'TN', 'Tunisia','216'],
			[227, 'TR', 'Turkey','90'],
			[228, 'TM', 'Turkmenistan','993'],
			[229, 'TC', 'Turks And Caicos Islands','1-649'],
			[230, 'TV', 'Tuvalu','688'],
			[231, 'UG', 'Uganda','256'],
			[232, 'UA', 'Ukraine','380'],
			[233, 'UM', 'United States Minor Outlying Islands',''],
			[234, 'UY', 'Uruguay','598'],
			[235, 'UZ', 'Uzbekistan','998'],
			[236, 'VU', 'Vanuatu','678'],
			[237, 'VA', 'Vatican City State (Holy See)','39-06'],
			[238, 'VE', 'Venezuela','58'],
			[239, 'VN', 'Vietnam','84'],
			[240, 'VG', 'Virgin Islands (British)',''],
			[241, 'VI', 'Virgin Islands (US)',''],
			[242, 'WF', 'Wallis And Futuna Islands','681'],
			[243, 'EH', 'Western Sahara','212'],
			[244, 'YE', 'Yemen','967'],
			[245, 'YU', 'Yugoslavia',''],
			[246, 'ZM', 'Zambia','260'],
			[247, 'ZW', 'Zimbabwe','263']

        ];
        return $countries;
    }
}
