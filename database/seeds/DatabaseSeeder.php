<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        /*factory(\App\Model\User::class, 5)->create()->each(function($u) {
            if($u->gender == 'female') {
                $u->detail()->save(factory(\App\Model\FUserDetail::class)->make());

                $rand = rand(0, 1);

                if($rand == 0) {
                    $u->comm()->save(factory(\App\Model\FComm::class)->make([
                        'userId2' => 4
                    ]));
                } else {
                    $u->commAlt()->save(factory(\App\Model\MComm::class)->make([
                        'userId1' => 4
                    ]));
                }
            } else {
                $u->detail()->save(factory(\App\Model\MUserDetail::class)->make());
            }

            $u->partnerPref()->save(factory(\App\Model\UserPartnerPref::class)->make());
        });*/

        
        $this->call([
			CountriesSeeder::class,
			EducationOptionsSeeder::class,
			JobOptionsSeeder::class,
			StarSeeder::class,
			LanguagesSeeder::class,
			OptionsSeeder::class,
			ReligionsSeeder::class,
			StatesSeeder::class,
		        DistrictAndCitiesSeeder::class,
			StatesAndDistrictsSeeder::class,
			PaymentSeeder::class,
		]);
    }
}

