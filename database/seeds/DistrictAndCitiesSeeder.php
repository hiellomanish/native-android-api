<?php

use Illuminate\Database\Seeder;

class DistrictAndCitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getAllDistrictsAndCities() as $key => $value) {
            DB::table('district_and_cities')->insert([
                'districtId' => $value[0],
				'taluk' => $value[1],
                'city' => $value[2],
                'soundex' => $value[3]
            ]);
        }
    }

    private function getAllDistrictsAndCities()
    {
        $DistrictsAndCities = [
			['266','Ambalappuzha','Alappuzha Municipality','K640, A412, A514, M521'],
			['266','Ambalappuzha','Komalapuram','K640, A412, A514, K541'],
			['266','Ambalappuzha','Mannanchery','K640, A412, A514, M552'],
			['266','Ambalappuzha','Pathirappally','K640, A412, A514, P361'],
			['266','Ambalappuzha','Ambalappuzha','K640, A412, A514'],
			['266','Ambalappuzha','Kalavoor','K640, A412, A514, K416'],
			['266','Ambalappuzha','Karumady','K640, A412, A514, K653'],
			['266','Ambalappuzha','Purakkad','K640, A412, A514, P623'],
			['266','Chengannur','Chengannur Municipality','K640, A412, C525, M521'],
			['266','Chengannur','Kurattissery','K640, A412, C525, K632'],
			['266','Chengannur','Mannar','K640, A412, C525, M560'],
			['266','Chengannur','Ala','K640, A412, C525, A400'],
			['266','Chengannur','Cheriyanad','K640, A412, C525, C653'],
			['266','Chengannur','Ennakkad','K640, A412, C525, E523'],
			['266','Chengannur','Mulakuzha','K640, A412, C525, M422'],
			['266','Chengannur','Pandanad','K640, A412, C525, P535'],
			['266','Chengannur','Puliyoor','K640, A412, C525, P460'],
			['266','Chengannur','Thiruvanvandoor','K640, A412, C525, T615'],
			['266','Chengannur','Venmony','K640, A412, C525, V550'],
			['266','Cherthala','Arookutty','K640, A412, C634, A623'],
			['266','Cherthala','Aroor','K640, A412, C634, A660'],
			['266','Cherthala','Cherthala Municipality','K640, A412, C634, M521'],
			['266','Cherthala','Ezhupunna','K640, A412, C634, E215'],
			['266','Cherthala','Kanjikkuzhi','K640, A412, C634, K522'],
			['266','Cherthala','Kodamthuruth','K640, A412, C634, K353'],
			['266','Cherthala','Kokkothamangalam','K640, A412, C634, K235'],
			['266','Cherthala','Kuthiathode','K640, A412, C634, K333'],
			['266','Cherthala','Muhamma','K640, A412, C634, M500'],
			['266','Cherthala','Pallippuram','K640, A412, C634, P416'],
			['266','Cherthala','Thaikattussery','K640, A412, C634, T232'],
			['266','Cherthala','Thanneermukkam','K640, A412, C634, T565'],
			['266','Cherthala','Vayalar','K640, A412, C634, V460'],
			['266','Cherthala','Cherthala North','K640, A412, C634, N630'],
			['266','Cherthala','Cherthala South','K640, A412, C634, S300'],
			['266','Cherthala','Kadakkarappally','K640, A412, C634, K326'],
			['266','Cherthala','Mararikkulam North','K640, A412, C634, M662, N630'],
			['266','Cherthala','Panavally','K640, A412, C634, P514'],
			['266','Cherthala','Pattanakkad','K640, A412, C634, P352'],
			['266','Cherthala','Perumbalam','K640, A412, C634, P651'],
			['266','Cherthala','Thuravoor Thekku','K640, A412, C634, T616, T200'],
			['266','Karthikappally','Cheppad','K640, A412, K632, C130'],
			['266','Karthikappally','Chingoli','K640, A412, K632, C524'],
			['266','Karthikappally','Haripad','K640, A412, K632, H613'],
			['266','Karthikappally','Kandalloor','K640, A412, K632, K534'],
			['266','Karthikappally','Karthikappally','K640, A412, K632'],
			['266','Karthikappally','Kayamkulam Municipality','K640, A412, K632, K524, M521'],
			['266','Karthikappally','Keerikkad','K640, A412, K632, K623'],
			['266','Karthikappally','Krishnapuram','K640, A412, K632, K625'],
			['266','Karthikappally','Kumarapuram','K640, A412, K632, K561'],
			['266','Karthikappally','Muthukulam','K640, A412, K632, M324'],
			['266','Karthikappally','Pathiyoor','K640, A412, K632, P360'],
			['266','Karthikappally','Puthuppally','K640, A412, K632, P314'],
			['266','Karthikappally','Arattupuzha','K640, A412, K632, A631'],
			['266','Karthikappally','Cheruthana','K640, A412, K632, C635'],
			['266','Karthikappally','Karuvatta','K640, A412, K632, K613'],
			['266','Karthikappally','Pallippad','K640, A412, K632, P413'],
			['266','Karthikappally','Thrikkunnapuzha','K640, A412, K632, T625'],
			['266','Karthikappally','Veeyapuram','K640, A412, K632, V165'],
			['266','Kuttanad','Champakkulam','K640, A412, K353, C512'],
			['266','Kuttanad','Edathua','K640, A412, K353, E330'],
			['266','Kuttanad','Kainakary North','K640, A412, K353, K526, N630'],
			['266','Kuttanad','Kainakary South','K640, A412, K353, K526, S300'],
			['266','Kuttanad','Kavalam','K640, A412, K353, K145'],
			['266','Kuttanad','Kunnumma','K640, A412, K353, K550'],
			['266','Kuttanad','Muttar','K640, A412, K353, M360'],
			['266','Kuttanad','Nedumudi','K640, A412, K353, N353'],
			['266','Kuttanad','Neelamperoor','K640, A412, K353, N451'],
			['266','Kuttanad','Pulinkunnu','K640, A412, K353, P452'],
			['266','Kuttanad','Ramankary','K640, A412, K353, R552'],
			['266','Kuttanad','Thakazhy','K640, A412, K353, T220'],
			['266','Kuttanad','Thalavady','K640, A412, K353, T413'],
			['266','Kuttanad','Veliyanad','K640, A412, K353, V453'],
			['266','Mavelikkara','Bharanikkavu','K640, A412, M142, B652'],
			['266','Mavelikkara','Chennithala','K640, A412, M142, C534'],
			['266','Mavelikkara','Kannamangalam','K640, A412, M142, K555'],
			['266','Mavelikkara','Kattanam','K640, A412, M142, K355'],
			['266','Mavelikkara','Mavelikkara Municipality','K640, A412, M142, M521'],
			['266','Mavelikkara','Thazhakara','K640, A412, M142, T226'],
			['266','Mavelikkara','Chunakkara','K640, A412, M142, C526'],
			['266','Mavelikkara','Noornad','K640, A412, M142, N653'],
			['266','Mavelikkara','Palamel','K640, A412, M142, P454'],
			['266','Mavelikkara','Perungala','K640, A412, M142, P652'],
			['266','Mavelikkara','Thamarakkulam','K640, A412, M142, T562'],
			['266','Mavelikkara','Thekkekara','K640, A412, M142, T226'],
			['266','Mavelikkara','Thriperumthura','K640, A412, M142, T616'],
			['266','Mavelikkara','Vallikunnam','K640, A412, M142, V425'],
			['266','Mavelikkara','Vettiyar','K640, A412, M142, V360'],
			['267','Aluva','Aluva Municipality','K640, E652, A410, M521'],
			['267','Aluva','Angamaly Municipality','K640, E652, A410, A525, M521'],
			['267','Aluva','Chengamanad','K640, E652, A410, C525'],
			['267','Aluva','Choornikkara','K640, E652, A410, C652'],
			['267','Aluva','Chowwara','K640, E652, A410, C600'],
			['267','Aluva','Edathala','K640, E652, A410, E334'],
			['267','Aluva','Kalady','K640, E652, A410, K430'],
			['267','Aluva','Kizhakkumbhagom','K640, E652, A410, K225'],
			['267','Aluva','Mattoor','K640, E652, A410, M360'],
			['267','Aluva','Nedumbassery','K640, E652, A410, N351'],
			['267','Aluva','Thekkumbhagom','K640, E652, A410, T251'],
			['267','Aluva','Vadakkumbhagom','K640, E652, A410, V325'],
			['267','Aluva','Ayyampuzha','K640, E652, A410, A512'],
			['267','Aluva','Karukutty','K640, E652, A410, K623'],
			['267','Aluva','Malayattoor','K640, E652, A410, M436'],
			['267','Aluva','Manjapra','K640, E652, A410, M521'],
			['267','Aluva','Mookkannoor','K640, E652, A410, M256'],
			['267','Aluva','Parakkadavu','K640, E652, A410, P623'],
			['267','Aluva','Thuravoor','K640, E652, A410, T616'],
			['267','Kanayannur','Amballur','K640, E652, K556, A514'],
			['267','Kanayannur','Cheranallur','K640, E652, K556, C654'],
			['267','Kanayannur','Kadamakkudy','K640, E652, K556, K352'],
			['267','Kanayannur','Kakkanad','K640, E652, K556, K253'],
			['267','Kanayannur','Kalamassery Municipality','K640, E652, K556, K452, M521'],
			['267','Kanayannur','Kanayannur','K640, E652, K556'],
			['267','Kanayannur','Kochi Municipal Corporation','K640, E652, K556, K200, M521, C616'],
			['267','Kanayannur','Kumbalam','K640, E652, K556, K514'],
			['267','Kanayannur','Kureekkad','K640, E652, K556, K623'],
			['267','Kanayannur','Manakunnam','K640, E652, K556, M525'],
			['267','Kanayannur','Maradu','K640, E652, K556, M630'],
			['267','Kanayannur','Mulamthuruthy','K640, E652, K556, M453'],
			['267','Kanayannur','Mulavukad','K640, E652, K556, M412'],
			['267','Kanayannur','Thiruvankulam','K640, E652, K556, T615'],
			['267','Kanayannur','Thrippunithura Municipality','K640, E652, K556, T615, M521'],
			['267','Kanayannur','Vazhakkala','K640, E652, K556, V224'],
			['267','Kanayannur','Edakkattuvayal','K640, E652, K556, E323'],
			['267','Kanayannur','Kaippattur','K640, E652, K556, K136'],
			['267','Kanayannur','Keecherry','K640, E652, K556, K260'],
			['267','Kanayannur','Kulayettikara','K640, E652, K556, K432'],
			['267','Kochi','Elamkunnapuzha','K640, E652, K200, E452'],
			['267','Kochi','Kochi Municipal Corporation','K640, E652, K200, M521, C616'],
			['267','Kochi','Kumbalangy','K640, E652, K200, K514'],
			['267','Kochi','Njarackal','K640, E652, K200, N262'],
			['267','Kochi','Puthuvype','K640, E652, K200, P311'],
			['267','Kochi','Chellanam','K640, E652, K200, C455'],
			['267','Kochi','Edavanakkad','K640, E652, K200, E315'],
			['267','Kochi','Kuzhuppilly','K640, E652, K200, K214'],
			['267','Kochi','Nayarambalam','K640, E652, K200, N651'],
			['267','Kochi','Pallippuram','K640, E652, K200, P416'],
			['267','Kothamangalam','Eramalloor','K640, E652, K355, E654'],
			['267','Kothamangalam','Kothamangalam Municipality','K640, E652, K355, M521'],
			['267','Kothamangalam','Kedavoor','K640, E652, K355, K316'],
			['267','Kothamangalam','Keerampara','K640, E652, K355, K651'],
			['267','Kothamangalam','Kottappady','K640, E652, K355, K313'],
			['267','Kothamangalam','Kuttamangalam','K640, E652, K355'],
			['267','Kothamangalam','Kuttampuzha','K640, E652, K355, K351'],
			['267','Kothamangalam','Neriamangalam','K640, E652, K355, N655'],
			['267','Kothamangalam','Pindimana','K640, E652, K355, P535'],
			['267','Kothamangalam','Pothanikkad','K640, E652, K355, P352'],
			['267','Kothamangalam','Thrikkariyoor','K640, E652, K355, T626'],
			['267','Kothamangalam','Varappetty','K640, E652, K355, V613'],
			['267','Kunnathunad','Chelamattom','K640, E652, K535, C453'],
			['267','Kunnathunad','Koovappady','K640, E652, K535, K113']

        ];

        return $DistrictsAndCities;
    }

}

