<?php

use \Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('login', 'API\AuthController@login');
Route::get('register', 'API\OptionController@index');
Route::post('register', 'API\AuthController@register');
Route::post('otp/send', 'API\AuthController@sendMailOTP');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', 'API\AuthController@user');
    Route::get('/verified', 'API\AuthController@verified');
    Route::post('/logout', 'API\AuthController@logout');
    Route::post('/register-device', 'API\FcmController@register');

    Route::get('/profile', 'API\UserController@profile');
    Route::post('/profile', 'API\UserController@updateProfile');
    Route::post('/profile/picture', 'API\UserController@uploadPhotos');
    Route::delete('/profile/picture', 'API\UserController@deletePhotos');

    Route::get('/profile/{id}/detail', 'API\UserController@profileDetail');
    Route::get('/profile/{id}/picture', 'API\UserController@getPhotos');

    Route::get('/overview', 'API\HomeController@overview');

    Route::get('/profile-visits/{type}', 'API\CommController@profileVisits')->where('type', 'visited-me|i-visited');
    Route::get('/interest-messages/{type}', 'API\CommController@interestMessages')->where('type', 'received|sent');
    Route::get('/photo-request/{type}', 'API\CommController@photoRequest')->where('type', 'received|sent');
    Route::get('/contact-request/{type}', 'API\CommController@contactRequest')->where('type', 'received|sent');

    Route::get('/shortlisted', 'API\CommController@shortlistedUsers');

    Route::post('/{userId}/shortlist', 'API\CommController@shortlist');
    Route::post('/{userId}/ignore', 'API\CommController@ignore');
    Route::delete('/{userId}/ignore', 'API\CommController@ignore');

    Route::post('/profile-visits/{userId}', 'API\CommController@storePV');
    Route::put('/profile-visits/{userId}/mark-as-read', 'API\CommController@markAsReadPV');

    Route::post('/interest-messages/{userId}', 'API\CommController@sendIM');
    Route::put('/interest-messages/{userId}/{status}', 'API\CommController@updateIMStatus')->where('status', 'accepted|rejected|pending');
    Route::delete('/interest-messages/{userId}', 'API\CommController@cancelIM');

    Route::post('/photo-request/{userId}', 'API\CommController@sendPR');
    Route::put('/photo-request/{userId}/{status}', 'API\CommController@updatePRStatus')->where('status', 'approved|rejected|pending');
    Route::delete('/photo-request/{userId}', 'API\CommController@cancelPR');

    Route::post('/contact-request/{userId}', 'API\CommController@sendCR');
    Route::put('/contact-request/{userId}/{status}', 'API\CommController@updateCRStatus')->where('status', 'approved|rejected|pending');
    Route::delete('/contact-request/{userId}', 'API\CommController@cancelCR');

    Route::get('/my-matches', 'API\MyMatchesController@index');
});
